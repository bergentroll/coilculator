/*
    Coilculator — calculator for vape coils

    Copyright (C) 2017 Anton Karmanov <bergentroll@insiberia.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Hash with material and special resistenses complainces
var spec_resists_list = {
    'Kanthal' : 1.25,
    'Nichrome Ni60' : 1.12,
    'Nichrome Ni80' : 1.13,
    'Nickel' : 0.087,
    'Stanless steel 316' : 0.75,
    'Titanium' : 0.6
};

function Calc() {
    // Get users input
    wires_num = eval(coilculator.wires_num.value);
    coils_num = eval(coilculator.coils_num.value);
    wire_d = eval(coilculator.wire_d.value);
    coil_d = eval(coilculator.coil_d.value);
    turns_num = eval(coilculator.turns_num.value);
    legs_l = eval(coilculator.legs_l.value);
    spec_resistsance = eval(coilculator.material.value);

    // Count output date
    length = 2 * legs_l + turns_num * Math.PI * (coil_d + wire_d / 2);
    wire_square = wires_num * Math.PI * (wire_d / 2) ** 2;
    total_resistance = length * (spec_resistsance / 1000) / (wire_square * coils_num);
    surf_square = Math.PI * wires_num * coils_num * wire_d * length;
    power = 0.20 * surf_square;

    // Write output data to page
    document.getElementById('total_length').innerHTML =
        length_string.replace(
            "$length", length.toFixed(2)
        ).replace(
            "$coils_num", coils_num * wires_num
        );
    document.getElementById('total_resistance').innerHTML =
        total_resistance_string.replace("$total_resistance", total_resistance.toFixed(2));
    document.getElementById('power').innerHTML =
        power_string.replace("$power", power.toFixed(2));
}

function Init() {
    // Generate select list
    var material = document.getElementById("material");
    var selectList = document.createElement("select");
    selectList.id = "material";
    material.appendChild(selectList);
    for (var key in spec_resists_list) {
        var option = document.createElement("option");
        option.value = spec_resists_list[key];
        option.text = key;
        selectList.appendChild(option);
    }

    // Get strings from page
    length_string = document.getElementById('total_length').innerHTML;
    total_resistance_string = document.getElementById('total_resistance').innerHTML;
    power_string = document.getElementById('power').innerHTML

    // Run calculation to write initial values
    Calc();
}

window.onload = Init;
