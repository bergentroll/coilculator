# Coilculator

Coilculator is a free simple calculator for vape coils. It reproduce
[repova](http://reprova.com/calc/) main functionality, but have some difference
in calculations.

You can check coilculator on https://bergentroll.gitlab.io/coilculator/.
